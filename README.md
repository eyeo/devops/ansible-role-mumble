# Mumble Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing [Mumble](https://mumble.info/) resources.
